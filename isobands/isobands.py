# -*- coding: utf-8 -*-

"""
TODO add on-the-fly profile generation for input raster datasets
     We need array members center coordinates to compute polygons
     Previously `rasterio` routines have been used, but here we need a generic solution
     that doesn't require having a geotiff dataset to generate geometric profile
      
""" 

import os
import geopandas as gp
import numpy as np
import pandas as pd
from contours.core import shapely_formatter as shapely_fmt
from contours.quad import QuadContourGenerator
from skimage.filters import gaussian

def _extract_polygon(src_data, dx, dy, minval, maxval):
    """
    """
    src_gauss = gaussian(np.where((src_data > minval)*(src_data<=maxval), 1, 0),
                         sigma=2.5,
                         preserve_range=True)

    c = QuadContourGenerator.from_rectilinear(dx,
                                              dy,
                                              np.flipud(src_gauss),
                                              shapely_fmt)

    contour = c.filled_contour(min=0.5, max=3.0)

    results = ({'properties': {'value': minval}, 'geometry': s}
               for i, s in enumerate(contour))
    geoms = list(results)
    return geoms


def extract_polygons(src_data, dx, dy, levels, maxval=101, src=None):
    """
    """

    gpd_prev = None
    gpd_diff = None

    for level in levels:
        geoms = _extract_polygon(src_data,
                                 dx,
                                 dy,
                                 level,
                                 maxval
                                 )

        gpd_current = gp.GeoDataFrame.from_features(geoms)
        gpd_current.crs = "ESPG:4326"

        if gpd_prev is None:
            gpd_prev = gpd_current
        else:
            gpd_diff = gp.overlay(gpd_current,
                                  gpd_prev,
                                  how='difference')
            gp_new = pd.concat([gpd_prev, gpd_diff], ignore_index=True)
            gpd_prev = gp_new.copy()

    try:
        return gp_new
    except:
        return gpd_current
