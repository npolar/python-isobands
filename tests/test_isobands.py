#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `isobands` package."""

import pytest

from click.testing import CliRunner

from isobands import isobands
from isobands import cli
from isobands import extract_polygons

import numpy as np

@pytest.fixture
def response():
    """Sample pytest fixture.

    See more at: http://doc.pytest.org/en/latest/fixture.html
    """
    # import requests
    # return requests.get('https://github.com/audreyr/cookiecutter-pypackage')


def test_content(response):
    """Sample pytest test function with the pytest fixture as an argument."""
    # from bs4 import BeautifulSoup
    # assert 'GitHub' in BeautifulSoup(response.content).title.string


def test_command_line_interface():
    """Test the CLI."""
    runner = CliRunner()
    result = runner.invoke(cli.main)
    assert result.exit_code == 0
    assert 'isobands.cli.main' in result.output
    help_result = runner.invoke(cli.main, ['--help'])
    assert help_result.exit_code == 0
    assert '--help  Show this message and exit.' in help_result.output


class PolygonTester():
    def __init__(self):
        self.src_data = np.random.rand(10,10)
        self.dx = np.arange(10)
        self.dy = np.arange(10)
        self.levels = [0.01, 0.02]

def test_extract_polygons_interface():
    tester = PolygonTester()
    polygons = None
    polygons = extract_polygons(tester.src_data,
                                tester.dx,
                                tester.dy,
                                tester.levels)
    assert polygons is not None
