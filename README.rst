===============
Python Isobands
===============


.. image:: https://img.shields.io/pypi/v/isobands.svg
        :target: https://pypi.python.org/pypi/isobands

.. image:: https://img.shields.io/travis/mitkin/isobands.svg
        :target: https://travis-ci.org/mitkin/isobands

.. image:: https://readthedocs.org/projects/isobands/badge/?version=latest
        :target: https://isobands.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




Python package to create fille polygons (isobands) from raster array


* Free software: MIT license
* Documentation: https://isobands.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
